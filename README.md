# README #

### What is this repository for? ###

Telegram bot written on Kotlin. Has several features like aggressive behaviour and double-link watching. Also have a command to count velocity of chat messages.
Ver. 0.1

### How do I get set up? ###

Import as gradle project into your favourite IDE.
One could run bot by starting function: 

main(array: Array<String>)

**For correct work ffmpeg is required to be installed on your machine.**

### Contribution guidelines ###

Please stick to this list:

* Create a separate branch
* Write tests/Implement functionality
* Create PR for code review