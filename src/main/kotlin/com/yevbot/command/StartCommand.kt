package com.yevbot.command

import com.yevbot.TextRes
import com.yevbot.constants.COMMAND_START_DESCRIPTION
import com.yevbot.constants.COMMAND_START_NAME
import com.yevbot.util.buildSimpleMessage
import org.telegram.telegrambots.api.objects.Chat
import org.telegram.telegrambots.api.objects.User
import org.telegram.telegrambots.bots.AbsSender
import org.telegram.telegrambots.bots.commands.BotCommand

class StartCommand : BotCommand(COMMAND_START_NAME, COMMAND_START_DESCRIPTION) {

    override fun execute(sender: AbsSender, user: User, chat: Chat, strings: Array<out String>) {
        sender.sendMessage(buildSimpleMessage(chat.id.toString(), TextRes.getString("ready")))
    }

}