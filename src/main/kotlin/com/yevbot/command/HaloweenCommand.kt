package com.yevbot.command

import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import com.yevbot.constants.COMMAND_HW_DESCRIPTION
import com.yevbot.constants.COMMAND_HW_NAME
import com.yevbot.model.Story
import com.yevbot.util.buildSimpleMessage
import org.telegram.telegrambots.api.objects.Chat
import org.telegram.telegrambots.api.objects.User
import org.telegram.telegrambots.bots.AbsSender
import org.telegram.telegrambots.bots.commands.BotCommand
import java.io.File
import java.io.InputStreamReader
import java.util.concurrent.ThreadLocalRandom

class HalloweenCommand: BotCommand(COMMAND_HW_NAME, COMMAND_HW_DESCRIPTION) {

    val random: ThreadLocalRandom = ThreadLocalRandom.current()
    val gson = Gson()

    override fun execute(sender: AbsSender, user: User, chat: Chat, params: Array<out String>?) {
        val storiesJson = File("stories.json")
        val stories = gson.fromJson<List<Story>>(InputStreamReader(storiesJson.inputStream()))

        sender.sendMessage(buildSimpleMessage(chat.id.toString(), stories[random.nextInt(0, stories.size)].story))
    }
}