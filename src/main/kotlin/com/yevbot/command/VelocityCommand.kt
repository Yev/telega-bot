package com.yevbot.command

import com.yevbot.PostsRepo
import com.yevbot.TextRes
import com.yevbot.constants.COMMAND_VELOCITY_DESCRIPTION
import com.yevbot.constants.COMMAND_VELOCITY_NAME
import com.yevbot.constants.MIN_MS
import com.yevbot.util.buildSimpleMessage
import com.yevbot.util.chatIdAsString
import org.telegram.telegrambots.api.objects.Chat
import org.telegram.telegrambots.api.objects.User
import org.telegram.telegrambots.bots.AbsSender
import org.telegram.telegrambots.bots.commands.BotCommand

class VelocityCommand(val repo: PostsRepo) : BotCommand(COMMAND_VELOCITY_NAME, COMMAND_VELOCITY_DESCRIPTION) {

    val DEFAULT_MIN = "10"

    override fun execute(sender: AbsSender, user: User, chat: Chat, params: Array<out String>?) {
        val minutes = parseParams(params)
        if (minutes <= 0) {
            sender.sendMessage(buildSimpleMessage(chat.chatIdAsString(), TextRes.getString("wrong_format")))
        } else {
            replyVelocityForLast(minutes, sender, chat.chatIdAsString())
        }
    }

    private fun parseParams(params: Array<out String>?) : Int {
        val param = if (params != null && params.isNotEmpty()) params[0] else DEFAULT_MIN
        try {
            return Integer.valueOf(param)
        } catch (e: NumberFormatException) {
            return -1
        }
    }

    private fun replyVelocityForLast(minutes: Int, sender: AbsSender, chatId: String) {
        val now = System.currentTimeMillis()
        val lastNmins = repo.getPostsTimestamps().filter { timestamp ->
            now - MIN_MS * minutes < timestamp
        }
        val message = String.format(TextRes.getString("posts_status"), minutes,
                lastNmins.size, lastNmins.size/minutes)

        sender.sendMessage(buildSimpleMessage(chatId, message))
    }

}