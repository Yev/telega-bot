package com.yevbot.command

import com.yevbot.TextRes
import com.yevbot.constants.COMMAND_STOP_DESCRIPTION
import com.yevbot.constants.COMMAND_STOP_NAME
import com.yevbot.util.buildSimpleMessage
import org.telegram.telegrambots.api.objects.Chat
import org.telegram.telegrambots.api.objects.User
import org.telegram.telegrambots.bots.AbsSender
import org.telegram.telegrambots.bots.commands.BotCommand

class StopCommand : BotCommand(COMMAND_STOP_NAME, COMMAND_STOP_DESCRIPTION) {

    override fun execute(sender: AbsSender, user: User, chat: Chat, array: Array<out String>?) {
        sender.sendMessage(buildSimpleMessage(chat.id.toString(), TextRes.getString("bye_message")))
    }
}