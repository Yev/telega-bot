package com.yevbot

interface PostsRepo {

    fun getPostsTimestamps() : List<Long>

}
