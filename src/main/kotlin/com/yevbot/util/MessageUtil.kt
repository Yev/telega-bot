package com.yevbot.util

import org.telegram.telegrambots.api.methods.ForwardMessage
import org.telegram.telegrambots.api.methods.send.SendAudio
import org.telegram.telegrambots.api.methods.send.SendMessage
import org.telegram.telegrambots.api.methods.send.SendVoice
import java.io.File


fun buildSimpleMessage(chatId: String, text: String) : SendMessage {
    val message = SendMessage()
    message.chatId = chatId
    message.text = text

    return message
}

fun buildForwardMessage(fromChatId: String, fromMessageId: Int, chatId: String): ForwardMessage {
    val msg = ForwardMessage()
    msg.fromChatId = fromChatId
    msg.messageId = fromMessageId
    msg.chatId = chatId

    return msg
}

fun buildReplyMessage(chatId: String, messageId: Int?, text: String = "") : SendMessage {
    val message = SendMessage()
    message.chatId = chatId
    message.text = text
    if (messageId != null) {
        message.replyToMessageId = messageId
    }

    return message
}

fun buildAudioReplyMessage(chatId: String, fromMessageId: Int, audioFile: File) : SendAudio {
    val sa = SendAudio()
    sa.chatId = chatId
    sa.setNewAudio(audioFile)
    sa.replyToMessageId = fromMessageId

    return sa
}

fun buildAudioReplyMessage(chatId: String, fromMessageId: Int, audioFileId: String?) : SendAudio {
    val sa = SendAudio()
    sa.chatId = chatId
    sa.audio = audioFileId
    sa.replyToMessageId = fromMessageId

    return sa
}

fun buildVoiceReplyMessage(chatId: String, messageId: Int, file: File): SendVoice {
    val sv = SendVoice()
    sv.chatId = chatId
    sv.replyToMessageId = messageId
    sv.setNewVoice(file)

    return sv
}

fun buildVoiceReplyMessage(chatId: String, messageId: Int, fileId: String?): SendVoice {
    val sv = SendVoice()
    sv.chatId = chatId
    sv.replyToMessageId = messageId
    sv.voice = fileId

    return sv
}
