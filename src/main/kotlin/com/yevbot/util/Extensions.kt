package com.yevbot.util

import org.telegram.telegrambots.api.objects.Chat
import org.telegram.telegrambots.api.objects.Message
import java.util.*

fun Message.chatIdAsString() : String {
    return this.chatId.toString()
}

fun Chat.chatIdAsString() : String {
    return this.id.toString()
}

fun ResourceBundle.getArray(propName: String, delim: String = "|") : List<String> {
    return this.getString(propName)?.split(delim)?.map(String::trim) ?: emptyList<String>()
}