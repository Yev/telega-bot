package com.yevbot

import com.yevbot.bot.YevBot
import com.yevbot.constants.DEFAULT_BUNDLE_NAME
import com.yevbot.resources.UTF8Control
import org.telegram.telegrambots.TelegramBotsApi
import org.telegram.telegrambots.exceptions.TelegramApiException
import org.telegram.telegrambots.logging.BotLogger
import org.telegram.telegrambots.logging.BotsFileHandler
import java.io.IOException
import java.util.*
import java.util.logging.ConsoleHandler
import java.util.logging.Level

val LOG_TAG = "Main"
val TextRes: ResourceBundle = ResourceBundle.getBundle(DEFAULT_BUNDLE_NAME, Locale("ru"), UTF8Control())

fun main(args: Array<String>) {

    configureBotLogger()

    val api = TelegramBotsApi()
    try {
        api.registerBot(YevBot())
    } catch (e: TelegramApiException) {
        BotLogger.error(LOG_TAG, "failed to register bot", e)
    }
}

fun configureBotLogger() {
    BotLogger.setLevel(Level.ALL)
    BotLogger.registerLogger(ConsoleHandler())
    try {
        BotLogger.registerLogger(BotsFileHandler())
    } catch (e: IOException) {
        BotLogger.severe(LOG_TAG, e)
    }
}