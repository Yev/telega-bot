package com.yevbot.model

import java.time.LocalDateTime

/**
 * Created by Yev on 10/18/16.
 */
data class Link (val messageId: Int,
                val chatId: Long,
                val link: String,
                val date: LocalDateTime = LocalDateTime.now()) {

    fun isSame(another: Link) : Boolean {
        return this.chatId == another.chatId && this.link.contentEquals(another.link)
    }

}