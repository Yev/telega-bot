package com.yevbot.constants

val MIN_MS = 1000 * 60
val ONE_HOUR_MS = MIN_MS * 60

val BOT_NAME = "yevbot"
val BOT_TOKEN = "255465365:AAFP1jTjSGT1GAti6TKEWiMjQOrOJrSO5WA"

val EMPTY = ""

val ENTITY_TYPE_URL = "url"

val DEFAULT_BUNDLE_NAME = "strings"

val COMMAND_START_NAME = "start"
val COMMAND_START_DESCRIPTION = "With this command you can start the Bot"
val COMMAND_STOP_NAME = "stop"
val COMMAND_STOP_DESCRIPTION = "With this command you can stop the Bot"
val COMMAND_VELOCITY_NAME = "vl"
val COMMAND_VELOCITY_DESCRIPTION = "With this command you can get information about chat velocity for last ten minutes"
val COMMAND_HW_NAME = "halloween"
val COMMAND_HW_DESCRIPTION = "Ask bot give you random Halloween story about Kobold Campers"