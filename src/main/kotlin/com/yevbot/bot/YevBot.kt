package com.yevbot.bot

import com.yevbot.PostsRepo
import com.yevbot.TextRes
import com.yevbot.command.HalloweenCommand
import com.yevbot.command.StartCommand
import com.yevbot.command.StopCommand
import com.yevbot.command.VelocityCommand
import com.yevbot.constants.*
import com.yevbot.model.Link
import com.yevbot.service.TtsService
import com.yevbot.util.*
import org.telegram.telegrambots.api.objects.Message
import org.telegram.telegrambots.api.objects.Update
import org.telegram.telegrambots.bots.TelegramLongPollingCommandBot
import org.telegram.telegrambots.logging.BotLogger
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.util.*
import java.util.concurrent.ThreadLocalRandom

class YevBot() : TelegramLongPollingCommandBot(), PostsRepo {

    companion object {
        val LOG_TAG: String = YevBot::class.java.simpleName
        val LINKS_LIMIT = 300
        val LINKS_DAYS_TTL: Int = 0
        val POSTS_TIMESTAMPS_TTL = ONE_HOUR_MS

        /* chances to be aggressive */
        val REPLY_RANGE_MAX_LIMIT = 100
        val CALM_BEHAVIOUR_RANGE: IntRange = 0..99
    }

    val lastHourPosts = ArrayList<Long>()
    val lastHundredLinks = ArrayDeque<Link>()
    val random: ThreadLocalRandom = ThreadLocalRandom.current()
    val aggressiveReplies = TextRes.getArray("jokes.elements")

    var fuckOffVoiceId: String? = null

    init {
        register(StartCommand())
        register(StopCommand())
        register(VelocityCommand(this))
        register(HalloweenCommand())

        registerDefaultAction { sender, message ->
            val commandUnknown = buildSimpleMessage(message.chatIdAsString(),
                    "Command ${message.text} is unknown to me. Try something else.")
            sender.sendMessage(commandUnknown)
        }
    }

    override fun getBotUsername(): String {
        return BOT_NAME
    }

    override fun getBotToken(): String {
        return BOT_TOKEN
    }

    override fun getPostsTimestamps(): List<Long> {
        return lastHourPosts
    }

    /**
     * Filters messages to the bot.
     *
     * @return <code>true</code> if should be filtered out; <code>false</code> - otherwise
     */
    override fun filter(message: Message): Boolean {
        return (message.isCommand && message.text?.contains("@yevbot") ?: false).not()
    }

    override fun processNonCommandUpdate(update: Update) {
        savePostTimestampAndCutOutdated()
        update.message?.let { msg ->
            //TODO: fix for replies on self messages; several links case
            if (msg.hasEntities()) {
                msg.entities.filter { entity -> entity.type.contentEquals(ENTITY_TYPE_URL) }
                        .forEach { entity -> processLink(msg, entity.text) }
            }

            if (checkMessageForBotOffense(msg)) {
                makeThemRespectYou(msg)
                return
            }

            tryReplyAggressively(msg)
        }
    }

    private fun savePostTimestampAndCutOutdated() {
        val now = System.currentTimeMillis()
        lastHourPosts.add(now)

        lastHourPosts.removeIf { time -> now - time > POSTS_TIMESTAMPS_TTL }
    }

    private fun processLink(message: Message, url: String?) {
        if (url == null) {
            BotLogger.warn(LOG_TAG, "processLink(); empty url")
            return
        }

        val link = Link(message.messageId, message.chatId, message.entities.first().text)
        val sameLink = lastHundredLinks.find { it.isSame(link) }
        if (sameLink != null) {
            replyAlreadyPosted(message, sameLink)
        } else {
            addLinkAndAdjustCollection(link)
        }
    }

    private fun replyAlreadyPosted(message: Message, sameLink: Link) {
        sendMessage(buildReplyMessage(message.chatIdAsString(), message.messageId,
                TextRes.getString("already_posted")))
        forwardMessage(buildForwardMessage(sameLink.chatId.toString(), sameLink.messageId,
                message.chatIdAsString()))
    }

    private fun addLinkAndAdjustCollection(link: Link) {
        lastHundredLinks.addLast(link)

        /* Clearing storage in order to not consume extra memory */
        while (lastHundredLinks.size > LINKS_LIMIT) {
            lastHundredLinks.removeFirst()
        }

        /* Removing old links */
        lastHundredLinks.removeAll {
            it.date.until(LocalDateTime.now(), ChronoUnit.DAYS) > LINKS_DAYS_TTL
        }
    }

    private fun checkMessageForBotOffense(message: Message): Boolean {
        if (!message.hasText()) {
            return false
        }

        val text = message.text

        //TODO: think of resources instead hardcode
        return text.contains("бот", true) && (text.contains("пидор", true) || text.contains("пидар", true))
    }

    private fun makeThemRespectYou(message: Message) {
        when (random.nextInt(0, 3)) {
            0 -> sendMessage(buildReplyMessage(message.chatIdAsString(), message.messageId,
                    TextRes.getString("wtf")))
            1 -> replySuckMyDick(message)
            else -> {
                if (fuckOffVoiceId == null) {
                    TtsService.instance.tts(TextRes.getString("fuck_off")) { file ->
                        if (file != null) {
                            val resp = sendVoice(buildVoiceReplyMessage(message.chatIdAsString(),
                                    message.messageId, file))
                            fuckOffVoiceId = resp?.voice?.fileId
                        } else {
                            sendMessage(buildReplyMessage(message.chatIdAsString(),
                                    message.messageId, TextRes.getString("wtf")))
                        }
                    }
                } else {
                    sendVoice(buildVoiceReplyMessage(message.chatIdAsString(), message.messageId,
                            fuckOffVoiceId))
                }
            }
        }
    }

    private fun replySuckMyDick(message: Message) {
        sendMessage(buildReplyMessage(message.chatIdAsString(), message.messageId,
                TextRes.getString("sosi.raz")))
        sendMessage(buildSimpleMessage(message.chatIdAsString(), TextRes.getString("sosi.dva")))
        sendMessage(buildSimpleMessage(message.chatIdAsString(), TextRes.getString("sosi.tri")))
    }

    private fun tryReplyAggressively(message: Message) {
        with(message) {
            val aggressiveReply = getAggressiveReply(aggressiveReplies)
            if (aggressiveReply.isNotEmpty()) {
                sendMessage(buildReplyMessage(chatIdAsString(), messageId, aggressiveReply))
            }
        }
    }

    internal fun getAggressiveReply(replies: List<String>): String {
        if (random.nextInt(0, REPLY_RANGE_MAX_LIMIT) in CALM_BEHAVIOUR_RANGE) {
            return EMPTY
        }

        return replies[random.nextInt(0, replies.size)]
    }

}