package com.yevbot.service

import com.github.kittinunf.fuel.httpGet
import java.io.ByteArrayInputStream
import java.io.File
import java.io.InputStream
import java.util.concurrent.TimeUnit

class TtsService private constructor() {

    private object Holder { val INSTANCE = TtsService() }

    companion object {
        val instance: TtsService by lazy { Holder.INSTANCE }

        private val ENDPOINT = "https://tts.voicetech.yandex.net/generate"
        private val YA_KEY = "ee16653d-5fa6-4047-87e3-e596064eaee5"
    }

    fun tts(text: String, language: String = "ru-RU", mood: String = "evil",
            handler: (file: File?) -> Unit) {
        val params = listOf(
                "text"    to text.replace(" ", "%20"),
                "format"  to "mp3",
                "speaker" to "omazh",
                "lang"    to language,
                "mood"    to mood,
                "key"     to YA_KEY
        )
        ENDPOINT.httpGet(params).response { req, resp, result ->
            if (result.component1() == null) handler.invoke(null)

            val input = File("nahui.mp3")
            input.deleteOnExit()
            input.copyInputStreamToFile(ByteArrayInputStream(resp.data))

            val ex = Runtime.getRuntime().exec("ffmpeg -i nahui.mp3 nahui.ogg")
            if (ex.waitFor(5, TimeUnit.SECONDS)) {
                val output = File("nahui.ogg")
                output.deleteOnExit()
                handler.invoke(output)
            } else {
                handler.invoke(input)
            }
        }
    }

    fun File.copyInputStreamToFile(inputStream: InputStream) {
        inputStream.use { input ->
            this.outputStream().use { fileOut ->
                input.copyTo(fileOut)
            }
        }
    }

}
